package ru.andreymarkelov.atlas.plugins;

import java.util.Map;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.ApplicationUsers;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.workflow.function.issue.AbstractJiraFunctionProvider;
import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.InvalidInputException;
import com.opensymphony.workflow.WorkflowException;

public class AssignPostFunction extends AbstractJiraFunctionProvider {
    private final CustomFieldManager cfMgr;
    private final UserManager userManager;

    public AssignPostFunction(
            CustomFieldManager cfMgr,
            UserManager userManager) {
        this.cfMgr = cfMgr;
        this.userManager = userManager;
    }

    @Override
    public void execute(Map transientVars, Map args, PropertySet ps) throws WorkflowException {
        MutableIssue issue = getIssue(transientVars);

        String cfId = (String) args.get(Consts.CUSTOM_FIELD_ID);
        if (!Utils.isValidStr(cfId)) {
            return;
        }

        try {
            Long.parseLong(cfId);
        } catch (NumberFormatException nex) {
            return;
        }

        CustomField customField = cfMgr.getCustomFieldObject(Long.parseLong(cfId));
        if (customField != null) {
            Object cfVal = issue.getCustomFieldValue(customField);
            if (cfVal == null) {
                throw new InvalidInputException(String.format("The field '%s' is required", customField.getName()));
            }

            String userName;
            if (cfVal instanceof ApplicationUser) {
                userName = ((ApplicationUser) cfVal).getName();
            } else if (cfVal instanceof User) {
                userName = ((User) cfVal).getName();
            } else {
                if (cfVal.toString().contains(":")) {
                    userName = cfVal.toString().substring(0, cfVal.toString().indexOf(":"));
                } else {
                    userName = cfVal.toString();
                }
            }

            ApplicationUser user = userManager.getUserByName(userName);
            if (user != null) {
                issue.setAssignee(ApplicationUsers.toDirectoryUser(user));
            } else {
                throw new InvalidInputException(String.format("The field '%s' must contain user", customField.getName()));
            }
        }
    }
}
