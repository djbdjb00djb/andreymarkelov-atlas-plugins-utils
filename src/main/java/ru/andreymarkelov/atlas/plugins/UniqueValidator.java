package ru.andreymarkelov.atlas.plugins;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import ru.andreymarkelov.atlas.plugins.amutils.workflow.validator.UniqueFieldValueValidatorFactory;

import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchResults;
import com.atlassian.jira.jql.builder.JqlQueryBuilder;
import com.atlassian.jira.jql.parser.DefaultJqlQueryParser;
import com.atlassian.jira.jql.parser.JqlParseException;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUsers;
import com.atlassian.jira.web.bean.PagerFilter;
import com.atlassian.query.Query;
import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.InvalidInputException;
import com.opensymphony.workflow.Validator;
import com.opensymphony.workflow.WorkflowException;

public class UniqueValidator implements Validator {
    private final CustomFieldManager customFieldManager;
    private final SearchService searchService;
    private final JiraAuthenticationContext authenticationContext;
    private final SimpleDateFormat dateFormat;

    public UniqueValidator(
            CustomFieldManager customFieldManager,
            SearchService searchService,
            JiraAuthenticationContext authenticationContext) {
        this.customFieldManager = customFieldManager;
        this.searchService = searchService;
        this.authenticationContext = authenticationContext;
        this.dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    }

    private boolean checkValue(Issue currentIssue, Issue queryIssue, String fieldName) {
        if (fieldName.startsWith("customfield")) {
            CustomField customField = customFieldManager.getCustomFieldObject(fieldName);
            if (customField != null) {
                Object currentIssueValue = currentIssue.getCustomFieldValue(customField);
                Object queryIssueValue = queryIssue.getCustomFieldValue(customField);
                if (currentIssueValue == null || queryIssueValue == null) {
                    return false;
                }
                return currentIssueValue.toString().equals(queryIssueValue.toString());
            }
        } else if (fieldName.equals("due")) {
            Timestamp currentIssueDueDate = currentIssue.getDueDate();
            Timestamp queryIssueDueDate = queryIssue.getDueDate();
            if (currentIssueDueDate == null || queryIssueDueDate == null) {
                return false;
            }
            return toDate(currentIssueDueDate).compareTo(toDate(queryIssueDueDate)) == 0;
        } else if (fieldName.equals("created")) {
            Timestamp currentIssueCreationDate = currentIssue.getCreated();
            Timestamp queryIssueCreationDate = queryIssue.getCreated();
            if (currentIssueCreationDate == null || queryIssueCreationDate == null) {
                return false;
            }
            return toDate(currentIssueCreationDate).compareTo(toDate(queryIssueCreationDate)) == 0;
        }
        return false;
    }

    private String toDate(Timestamp timestamp) {
        return dateFormat.format(new Date(timestamp.getTime())).toString();
    }

    @SuppressWarnings("rawtypes")
    @Override
    public void validate(
            Map transientVars,
            Map args,
            PropertySet ps) throws WorkflowException {
        Issue issue = (Issue) transientVars.get("issue");
        String cfId = (String) args.get(UniqueFieldValueValidatorFactory.ISSUEPROP);
        String jql = (String) args.get(UniqueFieldValueValidatorFactory.JQL);
        Object countObj = args.get(UniqueFieldValueValidatorFactory.COUNT);
        Integer count = (countObj != null) ? Integer.valueOf(countObj.toString()) : 0;
        String error = (String) args.get(UniqueFieldValueValidatorFactory.ERROR);

        String errorview;
        if (!Utils.isValidStr(error)) {
            errorview = authenticationContext.getI18nHelper().getText("ru.andreymarkelov.atlas.plugins.amutils.unique-validator.error");
        } else {
            errorview = String.format(error);
        }

        try {
            Query query = (new DefaultJqlQueryParser()).parseQuery(jql);
            if (issue.getKey() != null) {
                JqlQueryBuilder builder = JqlQueryBuilder.newBuilder();
                builder.where().addClause(query.getWhereClause()).and().not().issue(issue.getKey());
                builder.orderBy().setSorts(query.getOrderByClause().getSearchSorts());
                query = builder.buildQuery();
            }
            SearchResults results = searchService.search(ApplicationUsers.toDirectoryUser(authenticationContext.getUser()), query, PagerFilter.getUnlimitedFilter());
            if (results != null) {
                int realCount = 0;
                for (Issue i : results.getIssues()) {
                    if (checkValue(issue, i, cfId)) {
                        realCount++;
                    }
                }
                if (realCount > count) {
                    throw new InvalidInputException(errorview);
                }
            }
        } catch (SearchException e) {
            throw new InvalidInputException("Internal error in UniqueValidator");
        } catch (JqlParseException e) {
            throw new InvalidInputException("Internal error in UniqueValidator");
        }
    }
}
