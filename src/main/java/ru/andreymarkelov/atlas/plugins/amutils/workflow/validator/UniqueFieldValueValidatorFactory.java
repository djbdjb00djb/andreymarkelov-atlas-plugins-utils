package ru.andreymarkelov.atlas.plugins.amutils.workflow.validator;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import ru.andreymarkelov.atlas.plugins.Utils;

import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.plugin.workflow.AbstractWorkflowPluginFactory;
import com.atlassian.jira.plugin.workflow.WorkflowPluginValidatorFactory;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.opensymphony.workflow.loader.AbstractDescriptor;
import com.opensymphony.workflow.loader.ValidatorDescriptor;

public class UniqueFieldValueValidatorFactory extends AbstractWorkflowPluginFactory implements WorkflowPluginValidatorFactory {
    public static final String JQL = "jqlstr";
    public static final String COUNT = "issuecount";
    public static final String ERROR = "error";
    public static final String ISSUEPROP = "cfId";
    public static final String ISSUEPROPS = "issueProps";

    private final JiraAuthenticationContext authenticationContext;
    private final CustomFieldManager customFieldManager;

    public UniqueFieldValueValidatorFactory(
            JiraAuthenticationContext authenticationContext,
            CustomFieldManager customFieldManager) {
        this.authenticationContext = authenticationContext;
        this.customFieldManager = customFieldManager;
    }

    @Override
    public Map<String, Object> getDescriptorParams(Map<String, Object> validatorParams) {
        Map<String, Object> map = new HashMap<String, Object>();

        if (validatorParams != null && validatorParams.containsKey(JQL)) {
            map.put(JQL, extractSingleParam(validatorParams, JQL));
        } else {
            map.put(JQL, "");
        }

        if (validatorParams != null && validatorParams.containsKey(COUNT)) {
            String countStr = extractSingleParam(validatorParams, COUNT);
            map.put(COUNT, Utils.isPositiveInteger(countStr) ? Integer.valueOf(countStr) : Integer.valueOf(0));
        } else {
            map.put(COUNT, Integer.valueOf(0));
        }

        if (validatorParams != null && validatorParams.containsKey(ERROR)) {
            map.put(ERROR, extractSingleParam(validatorParams, ERROR));
        }

        if (validatorParams != null && validatorParams.containsKey(ISSUEPROP)) {
            map.put(ISSUEPROP, extractSingleParam(validatorParams, ISSUEPROP));
        } else {
            map.put(ISSUEPROP, "");
        }

        return map;
    }

    private Map<String, String> getFields() {
        Map<String, String> map = new LinkedHashMap<String, String>();
        map.put("due", authenticationContext.getI18nHelper().getText("ru.andreymarkelov.atlas.plugins.amutils.unique-validator.duedate"));
        map.put("created", authenticationContext.getI18nHelper().getText("ru.andreymarkelov.atlas.plugins.amutils.unique-validator.created"));
        for (CustomField field : customFieldManager.getCustomFieldObjects()) {
            map.put(field.getId(), field.getName());
        }
        return map;
    }

    private String getParam(AbstractDescriptor descriptor, String param) {
        if (!(descriptor instanceof ValidatorDescriptor)) {
            throw new IllegalArgumentException("Descriptor must be a ValidatorDescriptor.");
        }

        ValidatorDescriptor validatorDescriptor = (ValidatorDescriptor) descriptor;
        String value = (String) validatorDescriptor.getArgs().get(param);
        if (value!=null && value.trim().length() > 0) {
            return value;
        } else {
            return "";
        }
    }

    @Override
    protected void getVelocityParamsForEdit(Map<String, Object> velocityParams, AbstractDescriptor descriptor) {
        velocityParams.put(JQL, getParam(descriptor, JQL));
        velocityParams.put(COUNT, getParam(descriptor, COUNT));
        velocityParams.put(ERROR, getParam(descriptor, ERROR));
        velocityParams.put(ISSUEPROP, getParam(descriptor, ISSUEPROP));
        velocityParams.put(ISSUEPROPS, getFields());
    }

    @Override
    protected void getVelocityParamsForInput(Map<String, Object> velocityParams) {
        velocityParams.put(JQL, "");
        velocityParams.put(COUNT, 0);
        velocityParams.put(ERROR, "");
        velocityParams.put(ISSUEPROP, "");
        velocityParams.put(ISSUEPROPS, getFields());
    }

    @Override
    protected void getVelocityParamsForView(Map<String, Object> velocityParams, AbstractDescriptor descriptor) {
        velocityParams.put(JQL, getParam(descriptor, JQL));
        velocityParams.put(COUNT, getParam(descriptor, COUNT));
        velocityParams.put(ERROR, getParam(descriptor, ERROR));
        velocityParams.put(ISSUEPROP, getParam(descriptor, ISSUEPROP));
        velocityParams.put(ISSUEPROPS, getFields());
    }
}
