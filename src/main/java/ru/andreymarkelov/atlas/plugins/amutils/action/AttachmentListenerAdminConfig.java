package ru.andreymarkelov.atlas.plugins.amutils.action;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import ru.andreymarkelov.atlas.plugins.amutils.manager.GlobalSettingsManager;

import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.security.xsrf.RequiresXsrfCheck;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.web.action.JiraWebActionSupport;

public class AttachmentListenerAdminConfig extends JiraWebActionSupport {
    private static final long serialVersionUID = -1448499102564423477L;

    private final GlobalPermissionManager globalPermissionManager;
    private final ProjectManager projectManager;
    private final GlobalSettingsManager globalSettingsManager;

    private boolean isSaved = false;
    private List<Long> storedProjectIds;
    private String[] selectedProjectIds = new String[0];

    public AttachmentListenerAdminConfig(
            GlobalPermissionManager globalPermissionManager,
            ProjectManager projectManager,
            GlobalSettingsManager globalSettingsManager) {
        this.globalPermissionManager = globalPermissionManager;
        this.projectManager = projectManager;
        this.globalSettingsManager = globalSettingsManager;
    }

    @Override
    public String doDefault() throws Exception {
        if(!hasAdminPermission()) {
            return PERMISSION_VIOLATION_RESULT;
        }
        storedProjectIds = globalSettingsManager.getAttachmentListenerProjectIds();
        return INPUT;
    }

    @Override
    @RequiresXsrfCheck
    protected String doExecute() throws Exception {
        if (selectedProjectIds != null) {
            List<Long> ids = new ArrayList<Long>();
            for (String selectedProjectId : selectedProjectIds) {
                ids.add(Long.valueOf(selectedProjectId));
            }
            globalSettingsManager.setAttachmentListenerProjectsIds(ids);
        }
        setSaved(true);
        return getRedirect("AttachmentListenerAdminConfig!default.jspa?saved=true");
    }

    public Map<Long, String> getAllProjects() {
        Map<Long, String> projectMap = new TreeMap<Long, String>();
        for (Project project : projectManager.getProjectObjects()) {
            projectMap.put(project.getId(), project.getName());
        }
        return projectMap;
    }

    public String[] getSelectedProjectIds() {
        return selectedProjectIds;
    }

    public List<Long> getStoredProjectIds() {
        return storedProjectIds;
    }

    public boolean hasAdminPermission() {
        ApplicationUser user = getLoggedInApplicationUser();
        if (user == null) {
            return false;
        }
        return globalPermissionManager.hasPermission(Permissions.ADMINISTER, user);
    }

    public boolean isSaved() {
        return isSaved;
    }

    public void setSaved(boolean isSaved) {
        this.isSaved = isSaved;
    }

    public void setSelectedProjectIds(String[] selectedProjectIds) {
        this.selectedProjectIds = selectedProjectIds;
    }
}
