package ru.andreymarkelov.atlas.plugins.amutils.functions;

import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;

import com.atlassian.core.util.StringUtils;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.JiraDataType;
import com.atlassian.jira.JiraDataTypes;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchResults;
import com.atlassian.jira.jql.operand.QueryLiteral;
import com.atlassian.jira.jql.query.QueryCreationContext;
import com.atlassian.jira.plugin.jql.function.AbstractJqlFunction;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.util.MessageSet;
import com.atlassian.jira.util.MessageSetImpl;
import com.atlassian.jira.web.bean.PagerFilter;
import com.atlassian.query.Query;
import com.atlassian.query.clause.TerminalClause;
import com.atlassian.query.operand.FunctionOperand;

public class RelatedIssuesByQueryFunction extends AbstractJqlFunction {
    private static final Logger log = Logger.getLogger(RelatedIssuesByQueryFunction.class);

    private static final String SUBSTITUTION_TOKEN = "SELF_VALUE";

    private final SearchService searchService;
    private final PermissionManager permissionManager;
    private final JiraAuthenticationContext authenticationContext;

    public RelatedIssuesByQueryFunction(
            final SearchService searchService,
            final PermissionManager permissionManager,
            final JiraAuthenticationContext authenticationContext) {
        this.searchService = searchService;
        this.permissionManager = permissionManager;
        this.authenticationContext = authenticationContext;
    }

    private List<Issue> findIssues(User user, Query query) {
        SearchResults results;
        try {
            results = searchService.search(user, query, PagerFilter.getUnlimitedFilter());
        } catch (SearchException ex) {
            log.error("RelatedIssuesByQueryFunction::findIssues - searching error", ex);
            return null;
        }
        return results.getIssues();
    }

    @Override
    public JiraDataType getDataType() {
        return JiraDataTypes.ISSUE;
    }

    @Override
    public int getMinimumNumberOfExpectedArguments() {
        return 2;
    }

    @Override
    public List<QueryLiteral> getValues(
            QueryCreationContext context,
            FunctionOperand operand,
            TerminalClause terminalClause) {
        List<QueryLiteral> literals = new LinkedList<QueryLiteral>();

        String jql1 = operand.getArgs().get(0);
        String jql2 = operand.getArgs().get(1);

        log.info("RelatedIssuesByQueryFunction::getValues - JQL1 is " + jql1);
        SearchService.ParseResult parseResult = searchService.parseQuery(context.getUser(), jql1);
        if (!parseResult.isValid()) {
            log.error("RelatedIssuesByQueryFunction::getValues - JQL1 is not valid: " + jql1);
            return null;
        }
        List<Issue> issues = findIssues(context.getUser(), parseResult.getQuery());

        for (Issue issue : issues) {
            log.info("RelatedIssuesByQueryFunction::getValues - JQL1 issue " + issue.getKey());
            String resolvedJql = StringUtils.replaceAll(jql2, SUBSTITUTION_TOKEN, issue.getKey());
            log.info("RelatedIssuesByQueryFunction::getValues - JQL2 is " + resolvedJql);
            parseResult = searchService.parseQuery(context.getUser(), resolvedJql);
            if (!parseResult.isValid()) {
                log.error("RelatedIssuesByQueryFunction::getValues - JQL2 is not valid: " + resolvedJql);
                return null;
            }
            List<Issue> relatedIssues = findIssues(context.getUser(), parseResult.getQuery());
            for (Issue relatedIssue : relatedIssues) {
                log.info("RelatedIssuesByQueryFunction::getValues - JQL2 issue " + relatedIssue);
                if (relatedIssue != null && permissionManager.hasPermission(Permissions.BROWSE, relatedIssue, context.getUser())) {
                    literals.add(new QueryLiteral(operand, relatedIssue.getId()));
                }
            }
        }

        return literals;
    }

    @Override
    public MessageSet validate(
            User searcher,
            FunctionOperand operand,
            TerminalClause terminalClause) {
        MessageSet messages = new MessageSetImpl();

        List<String> keys = operand.getArgs();

        if (keys.size() != 2) {
            messages.addErrorMessage(authenticationContext.getI18nHelper().getText(
                    "ru.andreymarkelov.atlas.plugins.amutils.functions.RelatedIssuesByQueryFunction.error.args"));
            return messages;
        }

        SearchService.ParseResult parseResult = searchService.parseQuery(searcher, keys.get(0));
        if (!parseResult.isValid()) {
            messages.addErrorMessage(authenticationContext.getI18nHelper().getText(
                    "ru.andreymarkelov.atlas.plugins.amutils.functions.RelatedIssuesByQueryFunction.error.invalidjql1"));
        }

        parseResult = searchService.parseQuery(searcher, keys.get(1));
        if (!parseResult.isValid()) {
            messages.addErrorMessage(authenticationContext.getI18nHelper().getText(
                    "ru.andreymarkelov.atlas.plugins.amutils.functions.RelatedIssuesByQueryFunction.error.invalidjql2"));
        }

        return messages;
    }
}
