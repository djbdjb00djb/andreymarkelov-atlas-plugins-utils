package ru.andreymarkelov.atlas.plugins.amutils.functions;

import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.JiraDataType;
import com.atlassian.jira.JiraDataTypes;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchResults;
import com.atlassian.jira.jql.operand.QueryLiteral;
import com.atlassian.jira.jql.query.QueryCreationContext;
import com.atlassian.jira.plugin.jql.function.AbstractJqlFunction;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.util.MessageSet;
import com.atlassian.jira.util.MessageSetImpl;
import com.atlassian.jira.web.bean.PagerFilter;
import com.atlassian.query.clause.TerminalClause;
import com.atlassian.query.operand.FunctionOperand;

public class RelatedIssuesByFieldFunction extends AbstractJqlFunction {
    private static final Logger log = Logger.getLogger(RelatedIssuesByFieldFunction.class);

    private final SearchService searchService;
    private final PermissionManager permissionManager;
    private final CustomFieldManager customFieldManager;
    private final JiraAuthenticationContext authenticationContext;
    private final IssueManager issueManager;

    public RelatedIssuesByFieldFunction(
            final SearchService searchService,
            final PermissionManager permissionManager,
            final CustomFieldManager customFieldManager,
            final JiraAuthenticationContext authenticationContext,
            final IssueManager issueManager) {
        this.searchService = searchService;
        this.permissionManager = permissionManager;
        this.customFieldManager = customFieldManager;
        this.authenticationContext = authenticationContext;
        this.issueManager = issueManager;
    }

    @Override
    public JiraDataType getDataType() {
        return JiraDataTypes.ISSUE;
    }

    @Override
    public int getMinimumNumberOfExpectedArguments() {
        return 2;
    }

    @Override
    public List<QueryLiteral> getValues(
            QueryCreationContext context,
            FunctionOperand operand,
            TerminalClause terminalClause) {
        List<QueryLiteral> literals = new LinkedList<QueryLiteral>();

        String jql = operand.getArgs().get(0);
        String customFieldId = operand.getArgs().get(1);

        SearchService.ParseResult parseResult = searchService.parseQuery(context.getUser(), jql);
        if (parseResult.isValid()) {
            SearchResults results;
            try {
                results = searchService.search(context.getUser(), parseResult.getQuery(), PagerFilter.getUnlimitedFilter());
            } catch (SearchException e) {
                log.error("RelatedIssuesByFieldFunction::getValues - searching error", e);
                return null;
            }
            List<Issue> issues = results.getIssues();

            if (!issues.isEmpty()) {
                CustomField customField = customFieldManager.getCustomFieldObject(customFieldId);
                for (Issue issue : issues) {
                    Object fieldValue = customField.getValue(issue);
                    if (fieldValue != null) {
                        Issue relatedIssue = issueManager.getIssueObject(fieldValue.toString());
                        if (relatedIssue != null && permissionManager.hasPermission(Permissions.BROWSE, relatedIssue, context.getUser())) {
                            literals.add(new QueryLiteral(operand, relatedIssue.getId()));
                        }
                    }
                }
            }
        }

        return literals;
    }

    @Override
    public MessageSet validate(
            User searcher,
            FunctionOperand operand,
            TerminalClause terminalClause) {
        MessageSet messages = new MessageSetImpl();

        List<String> keys = operand.getArgs();

        if (keys.size() != 2) {
            messages.addErrorMessage(authenticationContext.getI18nHelper().getText(
                    "ru.andreymarkelov.atlas.plugins.amutils.functions.RelatedIssuesByFieldFunction.error.args"));
            return messages;
        }

        SearchService.ParseResult parseResult = searchService.parseQuery(searcher, keys.get(0));
        if (!parseResult.isValid()) {
            messages.addErrorMessage(authenticationContext.getI18nHelper().getText(
                    "ru.andreymarkelov.atlas.plugins.amutils.functions.RelatedIssuesByFieldFunction.error.invalidjql"));
        }

        CustomField customField = customFieldManager.getCustomFieldObject(keys.get(1));
        if (customField == null) {
            messages.addErrorMessage(authenticationContext.getI18nHelper().getText(
                    "ru.andreymarkelov.atlas.plugins.amutils.functions.RelatedIssuesByFieldFunction.error.notexistfield"));
        }

        return messages;
    }
}
